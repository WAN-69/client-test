
# Client Test Repository

Repository ini ditujukan hanya untuk test PT 62 Teknologi


## Setup

Clone the project

```bash
  git clone https://gitlab.com/WAN-69/client-test.git
```

Go to the project directory

```bash
  cd client-test
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run dev
```

