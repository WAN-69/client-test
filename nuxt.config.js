export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: "static",

  // Environment Variabel
  publicRuntimeConfig: {
    appUrl: process.env.APP_URL,
    baseUrl: process.env.BASE_URL,
    baseApiUrl: process.env.BASE_API_URL,
    fakeAvatarUrl: process.env.FAKE_AVATAR_URL,
    nameApp: process.env.NAME_APP
  },
  privateRuntimeConfig: {
    apiSecret: process.env.API_SECRET
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: process.env.NAME_APP,
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [
      // favicon ws diganti tappi gambare gepeng kik,masih butuh penyesuaian. baka pen coba tinggal ganti bae sizes'e atau dihapus bae.
      {
        rel: "icon",
        type: "image/x-icon",
        sizes: "32x40",
        href: "/icon.png",
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["@/assets/css/main.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "@/plugins/lodash"},
    { src: "@/plugins/vee-validate", mode: "client" },
    { src: "@/plugins/vue-final-modal", mode: "client" },
    { src: "@/plugins/vue-gates"},
    { src: "@/plugins/vue-viewer"},
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    "nuxt-vite",
    "@nuxt/postcss8",
    '@nuxtjs/moment',
  ],

  moment: {
    // defaultLocale: 'id',
    // locales: ['id'],
    defaultTimezone: 'Asia/Jakarta'
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    // https://go.nuxtjs.dev/pwa
    "@nuxtjs/pwa",
    "@nuxtjs/auth",
    ['nuxt-tailvue', {
      all: true,
      toast:{
        defaultProps: { 
          timeout: 3,
        } 
      }
    }],
  ],

  // Auth With NuxtAuth JWT
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: "login", method: "post", propertyName: "authorization.token" },
          user: { url: "user-profile", method: "get", propertyName: "user" },
          logout: { url: "logout", method: "post"},
        },
      },
    },
    redirect: {
      login: "/auth/login",
      // logout: "/auth/login",
      callback: "/",
      home: "/",
    },
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.BASE_API_URL,
    proxy: true,
  },

  proxy: {
    "/api/": {
      target: process.env.BASE_API_URL,
      pathRewrite: { "^/api/": "" },
      changeOrigin: true,
    },
  },

  router: {
    routeNameSplitter: "/",
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: "en",
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    },
    // Add exception
    transpile: ["vee-validate/dist/rules",'vue-final-modal'],
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      
    },
  },

  // Output Deploy
  buildDir: 'build-nuxt',

  // Loading bar
  // loading: '@/components/LoadingBar.vue', /* Custom Loading Bar
  loading: {
    color: "blue",
    height: "3px",
    throttle: 0,
  },

  loadingIndicator: {
    name: "three-bounce",
    color: "#3B8070",
    background: "white",
  },

  // Transition
  pageTransition: {
    name: "slide-right",
    mode: "out-in",
  },
  layoutTransition: "fade",

  // moment
  moment: {
    defaultLocale: 'id',
    locales: ['id'],
    defaultTimezone: 'Asia/Jakarta'
  }
};
