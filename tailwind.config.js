/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./modules/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "nuxt.config.js",
    "node_modules/tv-*/dist/tv-*.umd.min.js",
  ],
  theme: {
    extend: {
      fontFamily: {
        dmsans: ["DMSANS", "sans-serif"],
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ]
}
