import Vue from 'vue'
import Vuex from 'vuex'
import Meta from 'vue-meta'
import ClientOnly from 'vue-client-only'
import NoSsr from 'vue-no-ssr'
import { createRouter } from './router.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtError from '..\\layouts\\error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData, normalizeError } from './utils'
import { createStore } from './store.js'

/* Plugins */

import nuxt_plugin_plugin_4dad6d58 from 'nuxt_plugin_plugin_4dad6d58' // Source: .\\components\\plugin.js (mode: 'all')
import nuxt_plugin_modalclient_9fc77456 from 'nuxt_plugin_modalclient_9fc77456' // Source: .\\modal.client.js (mode: 'client')
import nuxt_plugin_toastclient_e9935e62 from 'nuxt_plugin_toastclient_e9935e62' // Source: .\\toast.client.js (mode: 'client')
import nuxt_plugin_button_4a204ae2 from 'nuxt_plugin_button_4a204ae2' // Source: .\\button.js (mode: 'all')
import nuxt_plugin_helper_27e75093 from 'nuxt_plugin_helper_27e75093' // Source: .\\helper.js (mode: 'all')
import nuxt_plugin_icon_9f8eb330 from 'nuxt_plugin_icon_9f8eb330' // Source: .\\icon.js (mode: 'all')
import nuxt_plugin_workbox_519c8ed3 from 'nuxt_plugin_workbox_519c8ed3' // Source: .\\workbox.js (mode: 'client')
import nuxt_plugin_metaplugin_a6fdff5a from 'nuxt_plugin_metaplugin_a6fdff5a' // Source: .\\pwa\\meta.plugin.js (mode: 'all')
import nuxt_plugin_iconplugin_9daeb472 from 'nuxt_plugin_iconplugin_9daeb472' // Source: .\\pwa\\icon.plugin.js (mode: 'all')
import nuxt_plugin_axios_64a51b77 from 'nuxt_plugin_axios_64a51b77' // Source: .\\axios.js (mode: 'all')
import nuxt_plugin_moment_3ea9e501 from 'nuxt_plugin_moment_3ea9e501' // Source: .\\moment.js (mode: 'all')
import nuxt_plugin_lodash_3538f289 from 'nuxt_plugin_lodash_3538f289' // Source: ..\\plugins\\lodash (mode: 'all')
import nuxt_plugin_veevalidate_346f6561 from 'nuxt_plugin_veevalidate_346f6561' // Source: ..\\plugins\\vee-validate (mode: 'client')
import nuxt_plugin_vuefinalmodal_ce87754a from 'nuxt_plugin_vuefinalmodal_ce87754a' // Source: ..\\plugins\\vue-final-modal (mode: 'client')
import nuxt_plugin_vuegates_b909aae6 from 'nuxt_plugin_vuegates_b909aae6' // Source: ..\\plugins\\vue-gates (mode: 'all')
import nuxt_plugin_vueviewer_3425f5a6 from 'nuxt_plugin_vueviewer_3425f5a6' // Source: ..\\plugins\\vue-viewer (mode: 'all')
import nuxt_plugin_plugin_215a55e6 from 'nuxt_plugin_plugin_215a55e6' // Source: .\\auth\\plugin.js (mode: 'all')

// Component: <ClientOnly>
Vue.component(ClientOnly.name, ClientOnly)

// TODO: Remove in Nuxt 3: <NoSsr>
Vue.component(NoSsr.name, {
  ...NoSsr,
  render (h, ctx) {
    if (process.client && !NoSsr._warned) {
      NoSsr._warned = true

      console.warn('<no-ssr> has been deprecated and will be removed in Nuxt 3, please use <client-only> instead')
    }
    return NoSsr.render(h, ctx)
  }
})

// Component: <NuxtChild>
Vue.component(NuxtChild.name, NuxtChild)
Vue.component('NChild', NuxtChild)

// Component NuxtLink is imported in server.js or client.js

// Component: <Nuxt>
Vue.component(Nuxt.name, Nuxt)

Object.defineProperty(Vue.prototype, '$nuxt', {
  get() {
    const globalNuxt = this.$root.$options.$nuxt
    if (process.client && !globalNuxt && typeof window !== 'undefined') {
      return window.$nuxt
    }
    return globalNuxt
  },
  configurable: true
})

Vue.use(Meta, {"keyName":"head","attribute":"data-n-head","ssrAttribute":"data-n-head-ssr","tagIDKeyName":"hid"})

const defaultTransition = {"name":"slide-right","mode":"out-in","appear":false,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

const originalRegisterModule = Vuex.Store.prototype.registerModule

function registerModule (path, rawModule, options = {}) {
  const preserveState = process.client && (
    Array.isArray(path)
      ? !!path.reduce((namespacedState, path) => namespacedState && namespacedState[path], this.state)
      : path in this.state
  )
  return originalRegisterModule.call(this, path, rawModule, { preserveState, ...options })
}

async function createApp(ssrContext, config = {}) {
  const router = await createRouter(ssrContext, config)

  const store = createStore(ssrContext)
  // Add this.$router into store actions/mutations
  store.$router = router

  // Create Root instance

  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    head: {"title":"PT.62-Teknologi","meta":[{"charset":"utf-8"},{"name":"viewport","content":"width=device-width, initial-scale=1"},{"hid":"description","name":"description","content":""},{"name":"format-detection","content":"telephone=no"}],"link":[{"rel":"icon","type":"image\u002Fx-icon","sizes":"32x40","href":"\u002Ficon.png"}],"style":[],"script":[]},

    store,
    router,
    nuxt: {
      defaultTransition,
      transitions: [defaultTransition],
      setTransitions (transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [transitions]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },

      err: null,
      dateErr: null,
      error (err) {
        err = err || null
        app.context._errored = Boolean(err)
        err = err ? normalizeError(err) : null
        let nuxt = app.nuxt // to work with @vue/composition-api, see https://github.com/nuxt/nuxt.js/issues/6517#issuecomment-573280207
        if (this) {
          nuxt = this.nuxt || this.$options.nuxt
        }
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in src/server.js
        if (ssrContext) {
          ssrContext.nuxt.error = err
        }
        return err
      }
    },
    ...App
  }

  // Make app available into store via this.app
  store.app = app

  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base, router.options.mode)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    store,
    route,
    next,
    error: app.nuxt.error.bind(app),
    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined,
    ssrContext
  })

  function inject(key, value) {
    if (!key) {
      throw new Error('inject(key, value) has no key provided')
    }
    if (value === undefined) {
      throw new Error(`inject('${key}', value) has no value provided`)
    }

    key = '$' + key
    // Add into app
    app[key] = value
    // Add into context
    if (!app.context[key]) {
      app.context[key] = value
    }

    // Add into store
    store[key] = app[key]

    // Check if plugin not already installed
    const installKey = '__nuxt_' + key + '_installed__'
    if (Vue[installKey]) {
      return
    }
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Object.prototype.hasOwnProperty.call(Vue.prototype, key)) {
        Object.defineProperty(Vue.prototype, key, {
          get () {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  // Inject runtime config as $config
  inject('config', config)

  if (process.client) {
    // Replace store state before plugins execution
    if (window.__NUXT__ && window.__NUXT__.state) {
      store.replaceState(window.__NUXT__.state)
    }
  }

  // Add enablePreview(previewData = {}) in context for plugins
  if (process.static && process.client) {
    app.context.enablePreview = function (previewData = {}) {
      app.previewData = Object.assign({}, previewData)
      inject('preview', previewData)
    }
  }
  // Plugin execution

  if (typeof nuxt_plugin_plugin_4dad6d58 === 'function') {
    await nuxt_plugin_plugin_4dad6d58(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_modalclient_9fc77456 === 'function') {
    await nuxt_plugin_modalclient_9fc77456(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_toastclient_e9935e62 === 'function') {
    await nuxt_plugin_toastclient_e9935e62(app.context, inject)
  }

  if (typeof nuxt_plugin_button_4a204ae2 === 'function') {
    await nuxt_plugin_button_4a204ae2(app.context, inject)
  }

  if (typeof nuxt_plugin_helper_27e75093 === 'function') {
    await nuxt_plugin_helper_27e75093(app.context, inject)
  }

  if (typeof nuxt_plugin_icon_9f8eb330 === 'function') {
    await nuxt_plugin_icon_9f8eb330(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_workbox_519c8ed3 === 'function') {
    await nuxt_plugin_workbox_519c8ed3(app.context, inject)
  }

  if (typeof nuxt_plugin_metaplugin_a6fdff5a === 'function') {
    await nuxt_plugin_metaplugin_a6fdff5a(app.context, inject)
  }

  if (typeof nuxt_plugin_iconplugin_9daeb472 === 'function') {
    await nuxt_plugin_iconplugin_9daeb472(app.context, inject)
  }

  if (typeof nuxt_plugin_axios_64a51b77 === 'function') {
    await nuxt_plugin_axios_64a51b77(app.context, inject)
  }

  if (typeof nuxt_plugin_moment_3ea9e501 === 'function') {
    await nuxt_plugin_moment_3ea9e501(app.context, inject)
  }

  if (typeof nuxt_plugin_lodash_3538f289 === 'function') {
    await nuxt_plugin_lodash_3538f289(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_veevalidate_346f6561 === 'function') {
    await nuxt_plugin_veevalidate_346f6561(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_vuefinalmodal_ce87754a === 'function') {
    await nuxt_plugin_vuefinalmodal_ce87754a(app.context, inject)
  }

  if (typeof nuxt_plugin_vuegates_b909aae6 === 'function') {
    await nuxt_plugin_vuegates_b909aae6(app.context, inject)
  }

  if (typeof nuxt_plugin_vueviewer_3425f5a6 === 'function') {
    await nuxt_plugin_vueviewer_3425f5a6(app.context, inject)
  }

  if (typeof nuxt_plugin_plugin_215a55e6 === 'function') {
    await nuxt_plugin_plugin_215a55e6(app.context, inject)
  }

  // Lock enablePreview in context
  if (process.static && process.client) {
    app.context.enablePreview = function () {
      console.warn('You cannot call enablePreview() outside a plugin.')
    }
  }

  // Wait for async component to be resolved first
  await new Promise((resolve, reject) => {
    // Ignore 404s rather than blindly replacing URL in browser
    if (process.client) {
      const { route } = router.resolve(app.context.route.fullPath)
      if (!route.matched.length) {
        return resolve()
      }
    }
    router.replace(app.context.route.fullPath, resolve, (err) => {
      // https://github.com/vuejs/vue-router/blob/v3.4.3/src/util/errors.js
      if (!err._isRouter) return reject(err)
      if (err.type !== 2 /* NavigationFailureType.redirected */) return resolve()

      // navigated to a different route in router guard
      const unregister = router.afterEach(async (to, from) => {
        if (process.server && ssrContext && ssrContext.url) {
          ssrContext.url = to.fullPath
        }
        app.context.route = await getRouteData(to)
        app.context.params = to.params || {}
        app.context.query = to.query || {}
        unregister()
        resolve()
      })
    })
  })

  return {
    store,
    app,
    router
  }
}

export { createApp, NuxtError }
