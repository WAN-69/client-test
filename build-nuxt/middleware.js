import $2fac71cb from '..\\middleware\\authenticate.js'
import $7762aba2 from '..\\middleware\\guest.js'

const middleware = {
  ['authenticate']: $2fac71cb,
  ['guest']: $7762aba2
}

export default middleware
