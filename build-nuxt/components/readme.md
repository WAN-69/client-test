# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Button>` | `<button>` (components/Button.vue)
- `<InputField>` | `<input-field>` (components/InputField..vue)
- `<LoadingBar>` | `<loading-bar>` (components/LoadingBar.vue)
- `<NuxtLogo>` | `<nuxt-logo>` (components/NuxtLogo.vue)
- `<Pagination>` | `<pagination>` (components/Pagination.vue)
- `<Tutorial>` | `<tutorial>` (components/Tutorial.vue)
- `<VTailwindModal>` | `<v-tailwind-modal>` (components/VTailwindModal.vue)
- `<IconsBack>` | `<icons-back>` (components/icons/Back.vue)
- `<IconsEmail>` | `<icons-email>` (components/icons/Email.vue)
- `<IconsKey>` | `<icons-key>` (components/icons/Key.vue)
- `<IconsLock>` | `<icons-lock>` (components/icons/Lock.vue)
- `<IconsUser>` | `<icons-user>` (components/icons/User.vue)
- `<IconsSidenavDashboard>` | `<icons-sidenav-dashboard>` (components/icons/sidenav/dashboard.vue)
- `<IconsSidenavUserManagement>` | `<icons-sidenav-user-management>` (components/icons/sidenav/user-management.vue)
- `<PagesProfileAccountProfile>` | `<pages-profile-account-profile>` (components/pages/profile/AccountProfile.vue)
- `<PagesProfileCredentials>` | `<pages-profile-credentials>` (components/pages/profile/Credentials.vue)
- `<PagesProfilePersonal>` | `<pages-profile-personal>` (components/pages/profile/Personal.vue)
- `<PagesProfileRiwayatPendidikan>` | `<pages-profile-riwayat-pendidikan>` (components/pages/profile/RiwayatPendidikan.vue)
- `<PagesProfileSkills>` | `<pages-profile-skills>` (components/pages/profile/Skills.vue)
