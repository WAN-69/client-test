export { default as Button } from '../..\\components\\Button.vue'
export { default as InputField } from '../..\\components\\InputField..vue'
export { default as LoadingBar } from '../..\\components\\LoadingBar.vue'
export { default as NuxtLogo } from '../..\\components\\NuxtLogo.vue'
export { default as Pagination } from '../..\\components\\Pagination.vue'
export { default as Tutorial } from '../..\\components\\Tutorial.vue'
export { default as VTailwindModal } from '../..\\components\\VTailwindModal.vue'
export { default as IconsBack } from '../..\\components\\icons\\Back.vue'
export { default as IconsEmail } from '../..\\components\\icons\\Email.vue'
export { default as IconsKey } from '../..\\components\\icons\\Key.vue'
export { default as IconsLock } from '../..\\components\\icons\\Lock.vue'
export { default as IconsUser } from '../..\\components\\icons\\User.vue'
export { default as IconsSidenavDashboard } from '../..\\components\\icons\\sidenav\\dashboard.vue'
export { default as IconsSidenavUserManagement } from '../..\\components\\icons\\sidenav\\user-management.vue'
export { default as PagesProfileAccountProfile } from '../..\\components\\pages\\profile\\AccountProfile.vue'
export { default as PagesProfileCredentials } from '../..\\components\\pages\\profile\\Credentials.vue'
export { default as PagesProfilePersonal } from '../..\\components\\pages\\profile\\Personal.vue'
export { default as PagesProfileRiwayatPendidikan } from '../..\\components\\pages\\profile\\RiwayatPendidikan.vue'
export { default as PagesProfileSkills } from '../..\\components\\pages\\profile\\Skills.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
